#semver cli
FROM node:18 as builder

WORKDIR /usr/app
RUN npm install semver && npm install -g pkg && git clone https://github.com/npm/node-semver.git && cd node-semver && pkg -t node18-linux bin/semver.js -o /usr/app/semver


#toolkit
FROM alpine
LABEL maintainer="Marc Herren <marc@remmen.io"

RUN apk update
RUN apk upgrade

# install tools & dependencies
RUN apk add bash jq git curl moreutils libstdc++ gcompat yamllint && rm -rf /var/cache/apk/*
RUN curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 && chmod +x /usr/local/bin/argocd
RUN cd /usr/local/bin && curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
RUN cd /usr/local/bin && curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && chmod 755 kubectl

COPY --from=builder /usr/app/semver /usr/local/bin
COPY tools/rancher /usr/local/bin